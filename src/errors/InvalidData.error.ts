import CustomError, { ErrorType } from "../CustomError"

export default class InvalidData extends CustomError {
  static type = "InvalidDataError" as ErrorType
}