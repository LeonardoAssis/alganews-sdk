import CustomError, { ErrorType } from "../CustomError"

export default class System extends CustomError {
  static type = "SystemError" as ErrorType
}