import CustomError, { ErrorType } from "../CustomError"

export default class ResourceNotFound extends CustomError {
  static type = "ResourceNotFoundError" as ErrorType
}