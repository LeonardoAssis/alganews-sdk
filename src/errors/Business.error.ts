import CustomError, { ErrorType } from "../CustomError"

export default class Business extends CustomError {
  static type = "BusinessError" as ErrorType
}