import CustomError, { ErrorType } from "../CustomError"

export default class InvalidParameter extends CustomError {
  static type = "InvalidParameterError" as ErrorType
}