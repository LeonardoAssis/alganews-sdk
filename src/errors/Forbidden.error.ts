import CustomError, { ErrorType } from "../CustomError"

export default class Forbiden extends CustomError {
  static type = "ForbidenError" as ErrorType
}