import CustomError, { ErrorType } from "../CustomError"

export default class Generic extends CustomError {
  static type = "GenericError" as ErrorType
}