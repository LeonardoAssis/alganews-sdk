import { User } from "../@types/User";
import { generateQueryString } from "../utils";
import Service from "./Services";

class UserService extends Service{
  
  static getDetailedUser (userId: number){
    return this.Http
      .get<User.Detailed>(`/users/${userId}`)
      .then(this.getData)
  }

  static updateExistingUser (userId: number, user: User.Input) {
    return this.Http
      .put<User.Detailed>(`/users/${userId}`, user)
      .then(this.getData)
  }

  static getAllUsers (search: User.QueryUser) {
    const queryString = generateQueryString(search)
    return this.Http
      .get<User.Summary[]>('/users'.concat(queryString))
      .then(this.getData)
  }

  static insertNewUser(user: User.Input) {
    return this.Http
      .post<User.Detailed>('/users', user)
      .then(this.getData)
  }

  static activateExistingUser (userId: number) {
    return this.Http
      .put<{}>(`/users/${userId}/activation`)
      .then(this.getData)
  }

  static deactivateExistingUser (userId: number) {
    return this.Http
      .delete<{}>(`/users/${userId}/activation`)
      .then(this.getData)
  }

  static getExistingEditor (editorId: number) {
    return this.Http
      .get<User.EditorDetailed>(`/users/editors/${editorId}`)
      .then(this.getData)
  }
  
  static getAllEditors (search: User.QueryEditor) {
    const queryString = generateQueryString(search)
    return this.Http
      .get<User.EditorSummary[]>('/users/editors'.concat(queryString))
      .then(this.getData)
  }
  
}

export default UserService