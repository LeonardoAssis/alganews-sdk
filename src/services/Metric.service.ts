import { Metrics } from "../@types/Metrics";
import Service from "./Services";

class MetricService extends Service {
  static getTop3Tags (){
    return this.Http
      .get<Metrics.EditorTagRatio>('/metrics/editor/top3-tags')
      .then(this.getData)
  }

  static getEditorMonthlyEarnings () {
    return this.Http
      .get<Metrics.EditorMonthlyEarnings>('/metrics/editor/monthly-earnings')
      .then(this.getData)
  }

  static getMonthlyRevenuesExpanses () {
    return this.Http
      .get<Metrics.MonthlyRevenuesExpenses>(
        '/metrics/monthly-revenues-expenses', 
        {
          headers: {
            'Content-Type': 'application/json'
          }
        })
      .then(this.getData)
  }

  static getMonthlyRevenuesExpansesChartJs () {
    return this.Http
      .get<Metrics.MonthlyRevenuesExpenses>(
        '/metrics/monthly-revenues-expenses', 
        {
          headers: {
            'Content-Type': 'application/vnd.alganews.chartjs+json'
          }
        })
      .then(this.getData)
  }
}

export default MetricService