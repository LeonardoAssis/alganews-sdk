import { CashFlow } from "../@types/CashFlow";
import { generateQueryString } from "../utils";
import Service from "./Services";

class CashFlowService extends Service {

  static getExistingEntry(entryId: number) {
    return this.Http
      .get<CashFlow.EntryDetailed>(`cashflow/entries/${entryId}`)
      .then(this.getData)
  }

  static updateExistingEntry(entryId: number, entry: CashFlow.EntryInput) {
    return this.Http
      .put<CashFlow.EntryDetailed>(`cashflow/entries/${entryId}`, entry)
      .then(this.getData)
  }

  static removeExistingEntry(entryId: number) {
    return this.Http
      .delete<{}>(`cashflow/entries/${entryId}`)
      .then(this.getData)
  }

  static getAllEntries(search: CashFlow.QueryEntry) {
    const queryString = generateQueryString(search)
    return this.Http
      .get<CashFlow.EntrySummary[]>('/cashflow/entries'.concat(queryString))
      .then(this.getData)
  }

  static insertNewEntry(entry: CashFlow.EntryInput) {
    return this.Http
      .put<CashFlow.EntryDetailed>('/cashflow/entries', entry)
      .then(this.getData)
  }

  static removeEntryBatch(entryIds: number[]){
    return this.Http
      .put<{}>('/cashflow/entries/bulk-removals')
      .then(this.getData)
  }

  static getExistingCategory(categoryId: number) {
    return this.Http
      .get<CashFlow.CategoryDetailed>(`/cashflow/categories/${categoryId}`)
      .then(this.getData)
  }

  static updateExistingCategory(categoryId: number, category: CashFlow.CategoryInput) {
    return this.Http
      .put<CashFlow.CategoryDetailed>(`/cashflow/categories/${categoryId}`, category)
      .then(this.getData)
  }

  static removeExistingCategory(categoryId: number){
    return this.Http
      .delete<{}>(`/cashflow/categories/${categoryId}`)
      .then(this.getData)
  }

  static getAllCategories(search: CashFlow.QueryCategory) {
    const queryString = generateQueryString(search)
    return this.Http
      .get<CashFlow.CategorySummary[]>('/cashflow/categories'.concat(queryString))
      .then(this.getData)
  }

  static insertNewCategory(category: CashFlow.CategoryInput) {
    return this.Http
      .post<CashFlow.CategoryDetailed>('/cashflow/categories', category)
      .then(this.getData)
  }

}

export default CashFlowService