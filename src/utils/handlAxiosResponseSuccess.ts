import { AxiosResponse } from "axios";
import { ErrorData } from "../CustomError";

export default function handleAxiosResponseSuccess<T>(response: AxiosResponse<T>){
  return response;
}